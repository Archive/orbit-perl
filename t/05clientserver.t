# Run the client/server example as a test
print "1..3\n";

my $srvpid = open(SRV, "perl server|");
if ( $srvpid ) {
    print "ok 1\n";
} else {
    print "ok 1\n";
}

sleep(1);
my $cln = system("perl client 2>&1");
if ( $cln ) {
    print "not ok 2\n";
} else {
    print "ok 2\n";
}

if (  $srvpid ) {
    kill(15,$srvpid);
}
my $close = close SRV;
if ( $close ) {
    print "not ok 3\n";
} else {
    print "ok 3\n";
}
